package com.example.runtimepermissionexample;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "PermissionExample";

    private Runnable mPermissionGrantedOperation;
    private final static int REQUEST_CAMERA_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button_change_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ボタンが押されたときのコールバック
                checkAppPermission(
                        REQUEST_CAMERA_PERMISSION,  // arg1: ActivityのRequestCode
                        Manifest.permission.CAMERA, // arg2: 必要な権限
                        new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG,"OP:change icon");
                                showLaunchCameraDialog("機能：アイコン変更","カメラで撮影した画像をアイコンに設定します。");
                            }
                        }, // arg3: 権限が付与されているときに実行したい処理
                        "アイコン画像を撮影するために、カメラを利用します" // arg4: 権限が拒否された場合に、2回め以降に追加で表示するダイアログの説明メッセージ
                );
            }
        });

        findViewById(R.id.button_post_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ボタンが押されたときのコールバック
                checkAppPermission(
                        REQUEST_CAMERA_PERMISSION,  // arg1: ActivityのRequestCode
                        Manifest.permission.CAMERA, // arg2: 必要な権限
                        new Runnable() {
                            @Override
                            public void run() {
                                // 権限が必要な、本来実行したい処理
                                Log.d(TAG,"OP:post photo");
                                showLaunchCameraDialog("機能：写真投稿","カメラで撮影した画像を投稿します。");
                            }
                        }, // arg3: 権限が付与されているときに実行したい処理
                        "写真投稿で使う画像を撮影するために、カメラを利用します"  // arg4: 権限が拒否された場合に、2回め以降に追加で表示するダイアログの説明メッセージ
                );
            }
        });

    }

    /**
     * アプリに権限が付与されているかどうかを判定し、
     * 必要ならば権限を要求する
     * @param
     * @param requestCode
     * @param checkPermission チェックする権限
     * @param permissionGrantedOperation 権限が付与されている場合に実行する処理
     * @param permissionExplanationMessage 権限を必要とする理由についての説明メッセージ
     */
    private synchronized void checkAppPermission(
            final int requestCode,
            @NonNull final String checkPermission,
            @NonNull final Runnable permissionGrantedOperation,
            @NonNull final String permissionExplanationMessage){
        // A: 権限が付与されているか確認する。
        if(PermissionChecker.checkSelfPermission(
                this, checkPermission) == PackageManager.PERMISSION_GRANTED){
            // 権限が付与されているので実行する
            Log.d(TAG, "checkAppPermission: GRANTED:"+checkPermission+", requestCode="+requestCode);
            this.mPermissionGrantedOperation = null;
            permissionGrantedOperation.run();

            return;
        }

        // B: 権限が付与されたあとに処理が続行できるように、処理を保存しておく
        this.mPermissionGrantedOperation = permissionGrantedOperation;

        // C: ユーザに対して権限についての説明が必要かどうか確認する。
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, checkPermission)) {
            Log.d(TAG, "checkAppPermission: shouldShowRequestPermissionRationale = true");
            // ユーザが一度権限を拒否しているので、権限についての説明のダイアログを表示する。
            new AlertDialog.Builder(this)
                    .setTitle("パーミッションを要求する理由")
                    .setMessage(permissionExplanationMessage)
                    .setCancelable(false)
                    .setNegativeButton(android.R.string.cancel, null)
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // 権限の説明ダイアログでOKが押された場合に、権限の要求を行う
                                    Log.d(TAG, "checkAppPermission: requestPermissions(dialog): "+ checkPermission +", requestCode="+ requestCode);
                                    ActivityCompat.requestPermissions(MainActivity.this,
                                            new String[]{checkPermission}, requestCode);
                                }
                            })
                    .create()
                    .show();
            return;
        }

        // D: 権限が付与されていないので、権限の要求を行う。
        Log.d(TAG, "checkAppPermission: requestPermissions: "+ checkPermission +", requestCode="+ requestCode);
        ActivityCompat.requestPermissions(this,
                new String[]{checkPermission},
                requestCode);

    }

    /**
     * 権限を要求した結果の処理。
     * OSの表示した権限の確認ダイアログの表示が終わると呼び出される。
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults) {

        if( grantResults.length == 0){
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if( requestCode != REQUEST_CAMERA_PERMISSION){
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        switch(requestCode){
            case REQUEST_CAMERA_PERMISSION:
            {
                processRequestPermissionResult(
                        requestCode,
                        Manifest.permission.CAMERA,
                        grantResults[0],
                        "アプリ情報＞許可でカメラへのアクセスを許可してください"
                );
                return;
            }
        }
    }

    /**
     * OSの権限の確認ダイアログの選択結果から次に行う処理を決める
     * @param requestCode
     * @param requestedPermission
     * @param grantResult
     * @param deniedDialogMessage
     */
    public void processRequestPermissionResult(
            final int requestCode,
            @NonNull final String requestedPermission,
            final int grantResult,
            @NonNull final String deniedDialogMessage) {
        // A: OSの権限確認ダイアログで、権限が付与されたか確認する。
        if ( grantResult == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "processRequestPermissionResult: GRANTED:" + requestedPermission + ", requestCode=" + requestCode);
            // 権限が付与されたので処理を実施する。
            executePermissionGrantedOperation();
            return;
        }

        // B: 権限が付与されてない。
        Log.d(TAG, "processRequestPermissionResult: DENIED:"+ requestedPermission +", requestCode="+requestCode);
        if ( ! ActivityCompat.shouldShowRequestPermissionRationale(this,
                requestedPermission)){
            // B-2: 今後表示しないが選択されているので、案内を出す
            Log.d(TAG, "processRequestPermissionResult: show app settings guide");
            new AlertDialog.Builder(this)
                    .setMessage(deniedDialogMessage)
                    .setNegativeButton("やめる",null)
                    .setPositiveButton("設定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            openAppSettings();
                        }
                    })
                    .create()
                    .show();
        }
    }

    /**
     * 保存されている、権限が付与されている場合の処理を実行する。
     */
    private synchronized void executePermissionGrantedOperation() {
        Log.d(TAG,"executePermissionGrantedOperation");
        final Runnable tmpPermissionGrantedOperation = this.mPermissionGrantedOperation;
        if( tmpPermissionGrantedOperation == null ){
            Log.d(TAG,"executePermissionGrantedOperation: mPermissionGrantedOperation is null.");
            return;
        }
        this.mPermissionGrantedOperation = null;
        tmpPermissionGrantedOperation.run();
    }

    /**
     * アプリの設定画面を開く
     */
    private void openAppSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        this.startActivity(intent);
    }

    /**
     * ダイアログを表示して、OKボタンが押されたらカメラを起動する
     * @param title ダイアログのタイトル
     * @param message ダイアログのメッセージ
     */
    private void showLaunchCameraDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // カメラを起動する
                        Intent intent = new Intent();
                        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        startActivityForResult(intent, 0);
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

}
